<div align="center">
  <h1>Builder</h1>
</div>

<div align="center">
  <img src="builder_icon.png" alt="drawing" width="250"/>
</div>

## Table of Contents

1. **[What is it?](#what-is-it)**
    1. [Real-World Analogy](#real-world-analogy)
    2. [Participants](#participants)
    3. [Collaborations](#collaborations)
2. **[When do you use it?](#when-do-you-use-it)**
    1. [Motivation](#motivation)
    2. [Known Uses](#known-uses)
    3. [Categorization](#categorization)
    4. [Aspects that can vary](#aspects-that-can-vary)
    5. [Solution to causes of redesign](#solution-to-causes-of-redesign)
    6. [Consequences](#consequences)
    7. [Relations with Other Patterns](#relations-with-other-patterns)
3. **[How do you apply it?](#how-do-you-apply-it)**
    1. [Structure](#structure)
    2. [Variations](#variations)
    3. [Implementation](#implementation)
4. **[Sources](#sources)**

<br>
<br>

## What is it?

> :large_blue_diamond: **Builder is a creational pattern for producing objects stepwise, so varying representations can
be created.**

### Real-World Analogy

_A made-to-order sandwich shop._

A customer (client) can either create a custom sandwich by choosing the different ingredients (steps in the production
process). The chef (builder) then assembles the ingredients into a sandwich (product). Or the customer can choose a
standard recipe from the menu (director).

### Participants

- :bust_in_silhouette: **Builder**
    - Defines an interface to:
        - `addPartA`: (aka `buildPartA`) set one product part representation.
        - `build`: (aka `getResult`) create the product object using the product representation.
- :man: **ConcreteBuilder**
    - Stores different product parts representation data
    - Provides implementation for:
        - `addPartA`
        - `build`
- :man: **Director**
    - Stores a Builder object
    - Provides implementation for:
        - `makeVariant` (aka `constructVariant`) call a number of add methods on builder (mutating its state).
- :man: **Product**
    - Product object to be constructed

### Collaborations

Either the client creates the **Director** object and configures it with the desired **Builder** object.
The **Director** tells the **Builder** which parts of the production process should be performed.
The **Builder** handles requests from the **Director** and adds parts to the **Product**.
The client retrieves the **Product** from the **Builder**.

Or the client instructs the **Builder** directly on which parts of the **Product** should be built.


<br>
<br>

## When do you use it?

> :large_blue_diamond: **When the construction algorithm might change and needs to be isolated from clients. Or, when
construction must allow different representations of an object.**

### Motivation

- How do we replace a constructor with many (optional) arguments?
- How do we create different representations (for example, when testing) of the same product?
- How can we abstract the construction of composite trees or other complex objects?

### Known Uses

- Object Configuration in Dependency Injection:
    - Configure and construct complex object graphs by defining how individual components are assembled.
        - For example: setting up an application with various services, environments and settings.
        - [ASP.NET WebApplicationBuilder](https://learn.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.builder.webapplicationbuilder?view=aspnetcore-8.0)
- Test case setup.
    - isolate tests from the constructor of class under tests (preventing rework when the constructor changes)
    - reduce duplication: only deviations from standard for tests need to be defined in arrange step
- Generating various reports:
    - For example, one report of users who have defaulted on overdue payments, including interest rate and a
      notification fee. And a second report about users that have any open payments (not only overdue payments).
- Document Object Model (DOM) Creation:
    - Constructing complex document structures like XML or HTML. Create nodes and elements in a step-by-step manner,
      simplifying handling nested structures.
- Database Query Building:
    - Constructing SQL queries programmatically, allowing for dynamic query generation based on varying criteria.
- Graphical User Interface (GUI) Construction:
    - Create complex UI components, especially when assembling elements that require multiple configurations or styles.
- Message Creation in Communication Protocols:
    - In networking and communication protocols, builders aid in constructing complex messages with different headers,
      body content, and metadata.
- State Machine Initialization:
    - Set up state machines by defining states, transitions, and conditions step by step.
- Workflow or Pipeline Construction:
    - Create complex workflows or data processing pipelines by adding and configuring individual steps or components.
- Mathematical Expression Builders:
    - Create expression trees by breaking down complex formulas into smaller components.
- Caching and Cache Configuration:
    - Configure caching systems by specifying caching strategies, expiration policies, and storage configurations.

### Categorization

Purpose:  **Creational**  
Scope:    **Object**   
Mechanisms: **Association**

Creational design patterns abstract the instantiation process.
They help make a system independent of how its objects are created, composed, and represented.
whereas an object creational pattern will delegate instantiation to another object.

There are two recurring themes in these patterns.
First, they all encapsulate knowledge about which concrete classes the system uses.
Second, they hide how instances of these classes are created and put together.
All the system at large knows about the objects is their interfaces as defined by abstract classes.
Consequently, the creational patterns give you a lot of flexibility in what gets created, who creates it, how it gets
created, and when.

### Aspects that can vary

- How a complex object gets created.

### Solution to causes of redesign

- Algorithmic dependencies.
    - Algorithms are often extended, optimized, and replaced, forcing dependants to also change.

### Consequences

| Advantages                                                                                                                                                                                                                                                                            | Disadvantages                                                                                                                                                                       |
|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| :heavy_check_mark: **Vary a product's internal representation.** <br> Because the product is constructed through an abstract interface,  The construction code is written once; then different Directors or clients can reuse it to build Product variants from the same set of parts | :x: **Violates Command Query Separation principle.** <br> It can confuse callers that builder methods mutate the builder and then return it as a result.                            |
| :heavy_check_mark: **Isolates code for construction and representation.** <br> Improves modularity by encapsulating the way a complex object is constructed and represented. Clients needn't know anything about the classes that define the product's internal structure.            | :x: **Possible inconsistent Builder state.** <br> A client might call build on a Builder who's state is inconsistent or still incomplete, resulting in an error.                    |
| :heavy_check_mark: **Fine control over the construction process.** <br>Builds the product step by step which gives finer control over the construction and consequently the internal structure of the resulting product                                                               | :x: **Director uses Builder as output argument.** <br> Output arguments are objects passed to a function to have their state changed. This can be unexpected behaviour for clients. |

### Relations with Other Patterns

_Distinction from other patterns:_

- Abstract Factory is similar to Builder in that it also can construct complex objects.
    - The Builder pattern focuses on constructing a complex object step by step.
    - Abstract Factory's emphasis is on families of product objects (either simple or complex).
    - Builder returns the product as a final step,
    - The Abstract Factory pattern returns the product immediately.

_Combination with other patterns:_

- A Composite is what the builder often builds

<br>
<br>

## How do you apply it?

> :large_blue_diamond: **A Builder class with methods for each step to configure the Product and a single build method
to create it. A Director that prepares the Builder to produce a "preset" Product.**

### Structure

```mermaid
classDiagram
    class Director {
        + makeVariant(): void
        - builder: Builder
    }

    class Builder {
        <<interface>>
        + addPartA(): void*
        + build(): Product
    }

    class ConcreteBuilder {
        + addPartA(): void
        + build(): Product
        - productPartARepresentation: any
        - productPartBRepresentation: any
    }

    class Product {
    }

    Director *-- Builder: composes
    Builder <|.. ConcreteBuilder: implements
    ConcreteBuilder --> Product: constructs
```

### Variations

- **Assembly and construction interface**: The builder returns child nodes to the director, which passes them back to
  the builder to create the parent nodes.
    - :heavy_check_mark: Sometimes needed to built tree structures bottom-up.
    - :x: Tighter coupling between director and builder.
- **Directly to Product**: instead of storing the product part representations, the Builder directly mutates the
  product.
    - :heavy_check_mark: Simpler.
    - :x: Only possible when the product supports incremental construction (e.g: a dataclass with defaults).

### Implementation

In the example we apply the builder pattern to configure an application:
turn on: caching (redis cache), infrastructure (database), validation, eventHandler, backgroundJobs, presentation (
controllers and swaggerGen), authenticationAndAuthorization.

You can have a builder for the real application, and one for testing. And a director to do the setup (using either
builder)

- [Interface (interface)]()
- [Concrete Oubject]()

The _"client"_ is a ... program:

- [Client]()

The unit tests exercise the public interface of ... :

- [Concrete Object test]()

<br>
<br>

## Sources

- [Design Patterns: Elements Reusable Object Oriented](https://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612)
- [Refactoring Guru - Builder](https://refactoring.guru/design-patterns/builder)
- [Head First Design Patterns: A Brain-Friendly Guide](https://www.amazon.com/Head-First-Design-Patterns-Brain-Friendly/dp/0596007124)
- [Gui Ferreira: The BEST Design Pattern for Maintainable Tests](https://youtu.be/pwj_sw31mfw?si=BRY24rw_oxCCE5VU)
- [ZoranHorvat: Master the Builder Pattern: The King of Creational Design Patterns in C# | ASP.NET Core Tutorial](https://www.youtube.com/watch?v=3p93Q6ivbZ4)
- [Geekific: The Builder Pattern Explained and Implemented in Java](https://youtu.be/MaY_MDdWkQw?si=j9dBREOmDWqRpPIt)
- [Milan Jovanović: Structuring Dependency Injection In ASP.NET Core The Right Way](https://youtu.be/tKEF6xaeoig?si=4DhJzFqH3LynKVeI)

<br>
<br>
